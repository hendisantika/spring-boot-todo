# Audited TodoList

Spring Boot todo list with Auditing

Application has two demo users: user/user (role=USER) and admin/admin (role=ADMIN)

![Login page](img/login.png "Login page")

![Welcome Page](img/welcome.png "Welcome Page")

![Add Todo Page](img/add.png "Add Todo Page")

![List Todo Page](img/list.png "List Todo Page")