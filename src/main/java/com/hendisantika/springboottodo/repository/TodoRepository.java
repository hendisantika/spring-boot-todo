package com.hendisantika.springboottodo.repository;

import com.hendisantika.springboottodo.entity.Todo;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-todo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/08/18
 * Time: 19.35
 * To change this template use File | Settings | File Templates.
 */
public interface TodoRepository extends CrudRepository<Todo, Long> {
}