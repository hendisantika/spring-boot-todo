package com.hendisantika.springboottodo.controller;

import com.hendisantika.springboottodo.entity.Todo;
import com.hendisantika.springboottodo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-todo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/08/18
 * Time: 07.03
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class TodoController {
    @Autowired
    private TodoRepository repository;

    @GetMapping(value = "/login")
    public String login() {
        return "login";
    }

    // Show all todos
    @GetMapping(value = "/todos")
    public String todoList(Model model) {
        model.addAttribute("todolist", repository.findAll());
        return "todos";
    }

    // Add new todo
    @GetMapping(value = "/add")
    public String addTodo(Model model) {
        model.addAttribute("todo", new Todo());
        return "addtodo";
    }

    // Save new todo
    @PostMapping(value = "/save")
    public String save(Todo todo) {
        repository.save(todo);
        return "redirect:todos";
    }

}